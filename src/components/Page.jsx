import React, { useState, useEffect } from "react";
import { js, html } from "../const/devWords";

export const Page = () => {
  const [code, setCode] = useState("");
  const [numberOfPresses, setNbOfPresses] = useState(0);
  const [playerLevel, setPlayerLevel] = useState({});

  useEffect(() => {
    setPlayerLevel({
      display: 1,
      percentage: 90,
      seuil: 1
    });
  }, []);

  const handleKeyboardInput = () => {
    if (numberOfPresses > playerLevel.seuil) {
      setPlayerLevel({
        display: playerLevel.display + 1,
        percentage: playerLevel.percentage - 10,
        seuil: playerLevel.seuil + playerLevel.seuil * 1.2
      });
    }
    setNbOfPresses(numberOfPresses + 1);
    if (numberOfPresses % 10 === 6) {
      setCode(`${code}
      `);
      return;
    }
    code.length > 1000
      ? setCode("")
      : setCode(`${code} ${js[Math.floor(Math.random() * js.length)]}`);
  };

  const calculateBugs = () => {
    return Math.round((numberOfPresses * playerLevel.percentage) / 100);
  };
  const calculateCleanCode = () => {
    return Math.round(
      numberOfPresses - (numberOfPresses * playerLevel.percentage) / 100
    ) > 0
      ? Math.round(
          numberOfPresses - (numberOfPresses * playerLevel.percentage) / 100
        )
      : 0;
  };
  return (
    <div>
      <p style={{ textAlign: "center" }}>
        Tapez 1000 lignes de codes valides pour creer un projet
      </p>
      <textarea
        value={code}
        placeholder={"Tapez votre code ici"}
        onKeyUp={handleKeyboardInput}
        style={{ width: "100%", height: 300 }}
      ></textarea>
      <p>Lignes de code tapees : {numberOfPresses}</p>
      <p style={{ color: "red" }}>Nombre de bugs : {calculateBugs()}</p>
      <p style={{ color: "lightgreen" }}>
        Nombre de lignes valides : {calculateCleanCode()}
      </p>
      <p>Experience en JS : {playerLevel.display}</p>
      <div style={{ border: "1px solid black", padding: "5px" }}>
        <p style={{ textAlign: "center" }}>Secret admin stats</p>
        <p>playerLevel.percentage : {playerLevel.percentage}</p>
        <p>playerLevel.seuil : {playerLevel.seuil}</p>
      </div>
    </div>
  );
};
